import { initializeApp, getApp, getApps } from "firebase/app"

let app

const firebaseConfig = {
  apiKey: "AIzaSyApsRQkKNm96MEQoMLJh5q2oojJJ-rNYIw",
  authDomain: "paytungan.firebaseapp.com",
  databaseURL:
    "https://paytungan-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "paytungan",
  storageBucket: "paytungan.appspot.com",
  messagingSenderId: "273409055774",
  appId: "1:273409055774:web:2df92a32022b9172e6e20f",
  measurementId: "G-03QGC8G9PJ",
}

if (getApps().length) {
  app = getApp()
} else {
  app = initializeApp(firebaseConfig)
}

export { app }
