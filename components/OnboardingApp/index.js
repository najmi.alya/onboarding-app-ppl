import React from "react"
import { css } from "twin.macro"

const OnboardingApp = props => {
  if (!props.show) {
    return null
  }
  return (
    <>
      <div
        tw="bg-hitam"
        css={css`
          position: fixed;
          opacity: 0.5;
          top: -5%;
          height: 110vh;
          width: 500px;
        `}
      ></div>
      <div tw="flex items-center justify-evenly rounded-t-2xl">
        <div
          tw="box-content p-5 rounded-[30px] w-[70%] max-w-[430px] -mt-20 border-white bg-white"
          css={css`
            position: fixed;
            top: 26%;
          `}
        >
          <div tw="grid justify-center ">
            <div tw="flex justify-center">
              <img src="/images/onboarding-app/first-step.png" tw="w-[70%] mx-8" alt="" />
            </div>
            <div tw="">
                <p tw="font-bold text-center text-2xl mt-6 mx-16">
                  {" "}
                  Semua Tagihan Dalam Satu Waktu{" "}
                </p>
                <p tw="text-center text-sm mt-4 mx-24">
                  {" "}
                  Seluruhnya akan tergabung menjadi dalam satu waktu untuk penagihan!{" "}
                </p>
            </div>
            <div tw="flex justify-center mt-8 mb-4" role="btn-next">
              <button
                  tw={
                    "rounded-lg justify-self-center font-medium bg-[#F6CB45] w-full h-full p-3 text-sm mx-16"
                  }
                  onClick={props.onClose}
                  >
                  {" "}
                  Selanjutnya
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default OnboardingApp
